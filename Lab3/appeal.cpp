#include "appeal.h"
#include <stdio.h>
#include <stdlib.h>

Appeal::Appeal() {}

void Appeal::printInfo() {
    cout << "Topic: " << qTopic << "." << endl;
    cout << "Content: " << qContent << "." << endl;
    string text = "Videos\n";
    bool torf = false;
    for(Video *video : videos){
        text += "Name: " + video->getName() + ", Topic: " + video->getTopic()
                + ", Format: " + video->getFormat() + "; \n";
        if(video != nullptr)
            torf = true;
    }
    text += "\n";
    if(torf == true)
    cout << text;

    torf = false;
    text = "Pictures\n";
    for(Picture *picture : pictures){
        text += "Name: " + picture->getName() + ", Format: " + picture->getFormat()
                + ", Height: " + to_string(picture->getHeight()) + ", Width: " + to_string(picture->getWidth()) + "; \n";
        if(picture != nullptr)
            torf = true;
    }
    text += "\n";
    if(torf == true)
    cout << text;

    text = "Files\n";
    for(UserFile *file : files){
        text += "Name: " + file->getName() + ", Format: " + file->getFormat()
                + ", Content: " + file->getContent() + "; \n";
        if(file != nullptr)
            torf = true;
    }
    text += "\n";
    if(torf == true)
    cout << text;
}

string Media::getName() {
    return name;
}

string Media::getFormat() {
    return format;
}

void Media::setName(string name) {
    this->name = name;
}

void Media::setFormat(string format) {
    this->format = format;
}

string Video::getTopic() {
    return topic;
}

void Video::setTopic(string topic) {
    this->topic = topic;
}

int Picture::getHeight() {
    return height;
}

int Picture::getWidth() {
    return width;
}

void Picture::setHeight(int height) {
    this->height = height;
}

void Picture::setWidth(int width) {
    this->width = width;
}

string UserFile::getContent() {
    return content;
}

void UserFile::setContent(string content) {
    this->content = content;
}

string Builder::getQTopic() {
    cout << "Enter your subject:\n";
    string text;
    getline(cin, text);
    return text;
}

string Builder::getQContent() {
    cout << "Enter the content of your appeal:\n";
    string text;
    getline(cin, text);
    return text;
}


vector<Video *> Builder::getVideos()
{
    cout << "Enter information about the videos\n";
    bool check = true;
    vector<Video *>videos;
    while(check){
        string name, format, topic;
        cout << "Enter the name: ";
        cin.ignore();
        getline(cin, name);
        cout << "Enter the format: ";
        getline(cin, format);
        cout << "Enter the topic: ";
        getline(cin, topic);
        Video *video = new Video();
        video->setName(name);
        video->setFormat(format);
        video->setTopic(topic);
        videos.push_back(video);
        while(true){
            cout << "Continue? (y/n)";
            string answer;
            cin >> answer;
            if(answer == "y"){
                break;
            }
            else if(answer == "n"){
                check = false;
                break;
            }
            else{
                cout << "Enter y/n !!!";
            }
        }
    }
    return videos;

}

vector<Picture *> Builder::getPictures()
{
    cout << "Enter information about the pictures\n";
    bool check = true;
    vector<Picture *> pics;
    while(check){
        string name, format;
        int height, width;
        cout << "Enter the name: ";
        cin.ignore();
        getline(cin, name);
        cout << "Enter the format: ";
        getline(cin, format);
        cout << "Enter the height: ";
        cin >> height;
        cout << "Enter the width: ";
        cin >> width;
        Picture *pic = new Picture();
        pic->setName(name);
        pic->setFormat(format);
        pic->setHeight(height);
        pic->setWidth(width);
        pics.push_back(pic);
        while(true){
            cout << "Continue? (y/n)";
            string answer;
            cin >> answer;
            if(answer == "y"){
                break;
            }
            else if(answer == "n"){
                check = false;
                break;
            }
            else{
                cout << "Enter y/n !!!";
            }
        }
    }
    return pics;
}

vector<UserFile *> Builder::getFiles()
{
    cout << "Enter information about the files\n";
    bool check = true;
    vector<UserFile *> files;
    while(check){
        string name, format, content;
        cout << "Enter the name: ";
        cin.ignore();
        getline(cin, name);
        cout << "Enter the format: ";
        getline(cin, format);
        cout << "Enter the content: ";
        getline(cin, content);
        UserFile *file = new UserFile();
        file->setName(name);
        file->setFormat(format);
        file->setContent(content);
        files.push_back(file);
        while(true){
            cout << "Continue? (y/n)";
            string answer;
            cin >> answer;
            if(answer == "y"){
                break;
            }
            else if(answer == "n"){
                check = false;
                break;
            }
            else{
                cout << "Enter y/n !!!";
            }
        }
    }
    return files;
}

Appeal *Director::getSection()
{
    Appeal * section = new Appeal();
    section->setQTopic(builder->getQTopic());
    section->setQContent(builder->getQContent());
    while(true){
    cout << "Do you want to add videos, screenshots or files?(y/n)";
    string answer;
    int option;
    bool check = true;
    cin >> answer;
    if(answer == "y"){
        cout << "Choose what to attach:\n1-Photo\n2-Video\n3-File\n";
        cin >> option;
        switch (option){
            case 1: section->setPic(builder->getPictures());
            break;
            case 2: section->setVideos(builder->getVideos());
            break;
            case 3: section->setFile(builder->getFiles());
            break;
            default: cout <<"Select 1,2 or 3 option!";
            break;
        }
    }else if (answer == "n"){
        check = false;
        break;
    }else{
        cout << "Enter y/n !!!";
    }
    }
    return section;
}
