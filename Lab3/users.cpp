#include "users.h"


Teacher::Teacher(long int userID, string topic,string studentId, string test_name, bool changeEnabled) {
    this->userID = userID;
    this->topic = topic;
    this->test_name = test_name;
    this->changeEnabled = changeEnabled;
    this->studentId=studentId;
}

void Teacher::infoAboutResult()
{
    cout << "Test result for Teacher\nSubject: " + topic + "\nTest name: " + test_name+ "\nStudent_id: " + studentId;
    cout << "\n*Test results*";
}

Student::Student(long int userID, string topic, string test_name)
{
    this->userID = userID;
    this->topic = topic;
 this->test_name = test_name;
}

void Student::infoAboutResult()
{
    cout << "Test result for Student\nSubject: " + topic + "\nTest-name: " + test_name;
    cout << "\n*Test results*";
}

User *StudentFactory::create()
{
    long int userID;
    string topic, test_name;
    cout << "Enter userID: ";
    cin >> userID;
    cout << "Enter the subject: ";
    cin.ignore();
    getline(cin, topic);
    cout << "Enter the test-name: ";
    cin.ignore();
    getline(cin, test_name);
    return new Student(userID, topic, test_name);
}

User *TeacherFactory::create()
{
    long int userID;
    string topic, content, student_id, result;
    bool changeEnabled = true;
    cout << "Enter userID: ";
    cin >> userID;
    cout << "Enter subject name: ";
    cin.ignore();
    getline(cin, topic);
    cout << "Enter student id: ";
    cin.ignore();
    getline(cin, student_id);
    cout << "Enter the test name: ";
    cin.ignore();
    getline(cin, content);
    if(changeEnabled == true)
    return new Teacher(userID, topic,student_id, content, changeEnabled);
    else cout << "The user has banned!";
}



Client::Client(Factory *f)
{
    factory = f;
}

void Client::printInfo()
{
    User *Message = factory->create();
    Message->infoAboutResult();

}

