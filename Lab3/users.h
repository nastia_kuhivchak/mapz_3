#ifndef USER_H
#define USER_H

#include <iostream>
#include <vector>

using namespace std;

class User
{
protected:
    long int userID;
    string topic;
    string test_name;
public:
    virtual void infoAboutResult() = 0;
    virtual ~User() = default;
};

class Student : public User
{
public:
    Student(long int userID, string topic, string test_name);
    void infoAboutResult();
};

class Teacher : public User
{
private:
    bool changeEnabled;
    string studentId;
public:
    Teacher(long int userID, string topic,string studentId, string test_name, bool changeEnabled);
    void infoAboutResult();
};


class Factory
{
public:
    virtual User *create() = 0;
};

class StudentFactory : public Factory
{
public:
    StudentFactory(){}
    User *create();
};

class TeacherFactory : public Factory
{
public:
    TeacherFactory(){}
    User *create();
};


class Client
{
private:
    Factory *factory;
public:
    Client(Factory *);
    void printInfo();
};
#endif // USER_H

