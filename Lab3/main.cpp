#include <QCoreApplication>
#include "singleton.h"
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Singleton *s = Singleton::getInstance();
    s->print();
    return a.exec();
}
