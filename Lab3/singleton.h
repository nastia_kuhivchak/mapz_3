#ifndef SINGLETON_H
#define SINGLETON_H


class Singleton
{
private:
   static Singleton * p_instance;
   Singleton() {}
   Singleton(const Singleton&);
   Singleton& operator = (Singleton&);
public:
   static Singleton * getInstance()
   {
       if(p_instance == nullptr)
           p_instance = new Singleton();
       return p_instance;
   }

   void print();
};

#endif // SINGLETON_H
