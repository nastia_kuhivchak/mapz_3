#ifndef QUESTION_H
#define QUESTION_H
#include <iostream>
#include <vector>
#include <string>

using namespace::std;

class Media {
protected:
    string name;
    string format;
public:
    string getName();
    string getFormat();
    void setName(string);
    void setFormat(string);
};

class Video : public Media {
private:
    string topic;
public:
    string getTopic();
    void setTopic(string);

};

class Picture : public Media {
private:
    int height;
    int width;
public:
    int getHeight();
    int getWidth();
    void setHeight(int);
    void setWidth(int);
};

class UserFile : public Media {
private:
    string content;
public:
    string getContent();
    void setContent(string);
};

class Appeal {
private:
    string qTopic;
    string qContent;
    vector<Video *> videos;
    vector<Picture *> pictures;
    vector<UserFile *> files;
public:
    Appeal();
    void printInfo();
    void setQTopic(string qTopic){
        this->qTopic = qTopic;
    }
    void setQContent(string qContent){
        this->qContent = qContent;
    }
    void setPic(vector<Picture*> pics){
        pictures = pics;
    }
    void setVideos(vector<Video *> vids){
        videos = vids;
    }
    void setFile(vector<UserFile *> files){
        this->files = files;
    }
};

class Builder
{
public:
    string getQTopic();
    string getQContent();
    vector<Video *> getVideos();
    vector<Picture *> getPictures();
    vector<UserFile *> getFiles();
};

class Director
{
    Builder *builder;
public:

    Appeal *getSection();
};

#endif // Appeal_H
