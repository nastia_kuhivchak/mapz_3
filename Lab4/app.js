
//фасад
class MyFacade {
    constructor(type) {
      this.type = type;
    }
        get(id) {
            switch (this.type) {
              case "audio": {
                return this._findAudio(id);
              }
              case "lecture": {
                return this._findLecture(id);
              }
              case "file": {
                return this._findFile(id);
              }
              case "book": {
                return this._findBook(id);
              }
              default: {
                throw new Error("No type set!");
              }
            }
          }
    
  _findAudio(id) {
    return 'audio '+id;
  }
  _findLecture(id) {
    return 'lecture '+id;
  }
  _findFile(id) {
    return 'file '+id;
  }
  _findBook(id) {
    return 'book '+id;
  }
}
const TYPE_AUDIO = "audio";
const TYPE_LECTURE = "lecture";
const TYPE_FILE = "file";
const TYPE_BOOK = "book";
const music = new MyFacade(TYPE_LECTURE);
console.log(music.get(3));


//легковаговик
class StudentPZ {
    constructor(fullName, group) {
        this.fullName = fullName;
        this.group = group;
    }
}
class StudentPZFactory {
    constructor() {
        this._students = [];
    }

    createSudentPZ(fullName, group) {
        let student = this.getStudent(fullName);
        if (student) {
            return student;
        } else {
            const newStudent = new StudentPZ(fullName, group);
            this._students.push(newStudent);
            return newStudent;
        }
    }

    getStudent(fullName) {
        return this._students.find(student => student.fullName === fullName);
    }
}

const factory = new StudentPZFactory();
const nastia = factory.createSudentPZ('Anastasiia Kuhivchak', 25);
const oksa = factory.createSudentPZ('Oksana Petryshyn', 25);
//const nastia2 = factory.createSudentPZ('Anastasiia Kuhivchak', 25); //не додасть
console.log(factory._students);



//компонувальник
class Component { 
    static countElems=0;
    constructor() {
        this.parentsNum = 0; 
        this.elemPrefix = ''; 
        this.elemsList = []; 
        this.id = Component.countElems; 
        Component.countElems++;
    }

showHierarchy() { 
        this.elemPrefix = this.setPrefixLength(this.parentsNum);
    }

    getChild(key) {
        return this.children[key]
    }
  
    setPrefixLength(count) { 
        let pre = '';
        for (let i = 0; i < count; i++) {
            pre += '----';
        }
        return pre;
    }
}

class AnyFile extends Component { 
    constructor(name) {
        super();
        this.name = name;
    }

    showHierarchy() {
        super.showHierarchy();
       console.log(this.elemPrefix+this.name);
    }
}

class Subject extends Component { 
    constructor(name) {
        super();
        this.name = name;
        this.children = []
    }

showHierarchy() {
        super.showHierarchy();
        console.log(this.elemPrefix+this.name);
        for (let i in this.children){
            this.children[i].showHierarchy();
        }
    }

    add(elem) {
        elem.parentsNum = this.parentsNum + 1;
        elem.parentId = this.id; 
        this.children.push(elem)
    }

    Remove(elem) {
        for (let i in this.children){
            if (this.children[i] === elem){
                this.children.splice(i, 1);
            }
        }
    }

    getChild(key) {
        return this.children[key]
    }
    getAllChildren(childrenArr) {
        if (!childrenArr) { 
            this.elemsList = [];
            childrenArr = this.children;
        }
        for (let i in childrenArr) {
            this.elemsList.push(childrenArr[i]);
            if (childrenArr[i].children) { 
                this.getAllChildren(childrenArr[i].children)
            }
        }
        return this.elemsList;
    }
}


let subjects = new Subject('Programming')
    subjects.add(new AnyFile('File'))
    subjects.add(new AnyFile('Lecture'))
    let math = new Subject('Math')
    subjects.add(math)
  let lin_alg = new Subject('Linear Algebra')
    math.add(lin_alg);
    lin_alg.add(new AnyFile('Lecture'))
    let kdm = new Subject('KDM')
    math.add(kdm);
    
    subjects.showHierarchy()