
//стратегія
let User = function(strategy) {
    this.strategy = strategy;
};
User.prototype.seeResult = function() {
    return this.strategy();
};
let studentStrategy = function() {
    console.log("Переглянути тільки свій результат");
};
let teacherStrategy = function() {
    console.log("Переглянути результати своїх студентів");
};
let adminStrategy = function() {
    console.log("Переглянути результати всіх");
};

let student = new User(studentStrategy);
let teacher = new User(teacherStrategy);
let admin = new User(adminStrategy);
 
student.seeResult();
teacher.seeResult();
admin.seeResult();

//cтан
let checked = function(){
   console.log('green');
};
let nochecked = function(){
   console.log('red');
}
let inProgress = function(){
    console.log('in progress');
}
class Test {
    constructor(transform) {
        this._transform = transform
    }
    
    setTransform(transform) {
        this._transform = transform
    }
    
    check() {
      this._transform()
        }
}

const test = new Test()

test.setTransform(checked);
test.check();
test.setTransform(nochecked);
test.check();
test.setTransform(inProgress);
test.check();


//посередник
let User1 = function (mediator, name) {
    this.mediator = mediator;
    this.name = name;
   };
   User1.prototype.send = function (msg) {
    console.log(this.name + ": sending appeal: " + msg);
    this.mediator.sendAppeal(msg, this);
   };
   User1.prototype.receive = function (msg) {
    console.log(this.name + ": received appeal: " + msg);
   };
   let myMediator = {
    users: [],
    addUser: function (user) {
     this.users.push(user);
    },
    sendAppeal: function (apl, user) {
     for (let i = 0; i < this.users.length; i++) {
      if (this.users[i] !== user) {
       this.users[i].receive(apl);
      }
     }
    }
   };
   
   let Adam = new User1(myMediator, "Adam");
   let Poll = new User1(myMediator, "Poll");
   myMediator.addUser(Adam);
   myMediator.addUser(Poll);
   Adam.send("I'm worried about my assessment, it's not fair. Review the work again");
   Poll.receive('Everything is written off, rejoice that it is not zero!!!!')
   